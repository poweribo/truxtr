Truxtr
======

This is a demo web application that tells the user what types of food trucks might be found near a specific location on a map.
It uses sfgov.org's data for [mobile food](https://data.sfgov.org/Permitting/Mobile-Food-Facility-Permit/rqzj-sfat) facility permit applications.

See the demo hosted at OpenShift : [Truxtr](http://truxtr-ibo.rhcloud.com/assets/index.html)

This is a full-stack application that uses Dropwizard, Redis and H2 on the backend and uses Backbone.JS, JQuery, Bootstrap CSS and Google Maps on the front-end.

Technology Choices
------------------

* I have a lot of experience using the Java language so I picked Java framework and Java-related tools.

* Dropwizard - I picked this because of its ease of configuration, deployment and less boilerplate code. Great replacement for Spring. No XML.
               Everything I needed is here (Jersey, Hibernate, Jackson to name a few) and documentation / community support is also very good.
               This is my first time using Dropwizard but had a lot of experience with Spring in the past.
* Guice - I wanted a lightweight IOC framework. I used the auto-wiring mechanism to detect dependencies.
          This is my first time using it but I have experience with others like Spring IOC, and Flex IOC frameworks like Swiz and Parsley.
          I liked how Guice does injections thru annotations, No XML, Less verbose, no XML, no reliance with string identifiers and Bob Lee.
* Hibernate - I wanted to use an ORM to speed up writing CRUD operations and so that I dont have to write db-specific queries.
              I have used Hibernate before and others like iBatis/myBatis, JPA.
* Redis - My first time using Redis or NoSQL for that matter and wanted to leverage it for fast lookups for the auto-suggest widget.
* H2 - I used H2 because it is so portable and great for caching small datasets and very fast. I have used many other embedded dbs similar to H2.
       I have experience with many other relational database like Oracle, DB2, Sybase, MSSQL, Postgres, MySQL etc.
* Backbone.JS - Used for structuring the front-end JS code. I've used other MVC framework in the past like YUI and little bit of CanJS.
               This is my first time using Backbone JS. I have transitioned from Struts/JSP/HTML to Flex/AS3 world many years ago so this is
               a nice refresher for me.
* JQuery - Backbone dependencies.
* Google Maps API - Required. Have used it in the past for a bunch of POC applications.
* SODA API - Required. First time using SODA API.
* OpenShift Cloud Hosting - I picked OpenShift because it is one of the first SAAS hosting that fully supported Java/JavaEE and DIY projects.
                            It has a great api, web admin UI, excellent documentation and support. Also free for first 3 gears.
                            It provides Jenkins (continuous integration tool) hooked to your custom action hooks (build, start, stop, deploy),
                            and a git CVS repo. My first time using Openshift but have used Google App Engine in the past.
                            Also, the free plan is of the least restrictive plan for developers who just wanted to hack some small projects.
* Maven - For building my project. With Dropwizard, it creates a single jar file and auto-downloads dependencies. Have used Maven many times before.
          I have used other build tools like Gradle, Ant Ivy, Leiningen. I also provided an equivalent Gradle script.

Trade-Offs / Possible enhancements
----------------------------------

* The use of H2 is only a quick and dirty way of caching items instead of querying the SODA Api directly. I wanted the results
  to be displayed quickly. May not be wise to continue using it if the dataset grows a lot bigger. It also uses the default
  column sizes of varchar(255). Also unit test for persistence wont be useful until H2 is switched from "mem" to regular FS db.

* The polling service that populates H2/Redis does a delete all and bulk insert (no updates). Could result into blank results if user does
  a search while bulk inserts are happening in the background. Could have used a better strategy.

* For real production code, I would split the JS files into sub-components and use Browserify and Gulp to perform the usual
  pre-processing like concatenation, minification, jshint etc.

* If I had more time, I would replace Backbone JS views with ReactJS for a better performance.

* If I had more time, I'd add schedules as part of criteria when displaying Search results.

* Support other data sources for food truck locations in other cities (ie NYC)

* Enhancements for front-end: auto-zoom to markers, bigger map layout, better UX

* Write more tests

Requirements to build and run
-----------------------------

This project includes configuration needed to deploy on Openshift. Pls see .openshift folder for action hooks.

To compile and run locally, clone this repo and do the ff steps :

1. install redis w/ default settings
2. make sure you have Java 7 JDK and Maven
3. mvn package
4. java -jar target/truckotracker-1.0-SNAPSHOT.jar server truckotracker.yml
5. enter http://localhost:8080/assets/index.html on your browser

Other Projects
--------------

I dont have any other OSS projects :). I would gladly open-source a sponsored project.

About Me
--------

I'm a professional software developer with a lot of experience writing enterprise applications. Full-stack developer and can do some UX too.
Currently using Java, JS, Flex, SQL. Current interests: Clojurescript, ReactJS, Scala, Play, Akka, Phonegap, CanJS, Dota2
I love writing software and learning new stuff. You can find out more about me at [Linkedin]
(https://www.linkedin.com/profile/view?id=2312701)